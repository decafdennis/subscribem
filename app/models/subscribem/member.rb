module Subscribem
  class Member < ActiveRecord::Base
    belongs_to :account
    belongs_to :user
  end
end
