module Subscribem
  class Account < ActiveRecord::Base
    belongs_to :owner, :class_name => 'User'
    has_many :members
    has_many :users, :through => :members
    accepts_nested_attributes_for :owner
    
    before_validation do
      self.subdomain = subdomain.to_s.downcase
    end
    
    validates :subdomain, :presence => true, :uniqueness => true
    validates_exclusion_of :subdomain, :in => ['admin'], :message => 'is not allowed. Please choose another subdomain.'
    validates_format_of :subdomain, :with => /\A[\w\-]+\Z/i, :message => 'is not allowed. Please choose another subdomain.'
    
    def self.create_with_owner(params = {})
      account = new(params)
      if account.save
        account.users << account.owner
      end
      account
    end
    
    def create_schema
      Apartment::Database.create(subdomain)
    end
  end
end
