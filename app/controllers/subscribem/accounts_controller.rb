require_dependency 'subscribem/application_controller'

module Subscribem
  class AccountsController < ApplicationController
    def new
      @account = Account.new
      @account.build_owner
    end

    def create
      @account = Account.create_with_owner(account_params)

      if @account.valid?
        force_authentication!(@account.owner)
        @account.create_schema
        redirect_url = subscribem.root_url(:subdomain => @account.subdomain)
        redirect_to redirect_url, :notice => 'Your account has been successfully created.'
      else
        flash[:error] = 'Sorry, your account could not be created.'
        render :new
      end
    end
    
    private
    
    def account_params
      params.require(:account).permit(:name, :subdomain, :owner_attributes => [
        :email, :password, :password_confirmation
      ])
    end
  end
end
