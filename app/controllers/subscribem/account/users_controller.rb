require_dependency 'subscribem/application_controller'

module Subscribem
  class Account::UsersController < ApplicationController
    def new
      @user = User.new
    end

    def create
      account = Account.find_by!(:subdomain => request.subdomain)
      user = account.users.create(user_params)
      force_authentication!(user)
      redirect_to root_path, notice: 'You have signed up successfully.'
    end

    private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
  end
end
