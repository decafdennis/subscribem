# Gems do not autoload in engines because they're in the gemspec.
require 'warden'
require 'dynamic_form'
require 'apartment'

module Subscribem
  class Engine < Rails::Engine
    isolate_namespace Subscribem
    
    config.generators do |g|
      g.test_framework :rspec, :view_specs => false
    end

    config.to_prepare do
      # Needs .rb extension for #load.
      ext = 'subscribem/application_controller_ext.rb'
      Rails.configuration.cache_classes ? require(ext) : load(ext)
    end
  end
end
