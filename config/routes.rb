require 'subscribem/constraints/subdomain_required'

Subscribem::Engine.routes.draw do
  # Routes within the scope of an account. Differentiate with the routes below using the subdomain instead of a prefix.
  scope module: :account, as: :account, constraints: Subscribem::Constraints::SubdomainRequired do
    root to: 'dashboard#index'
    # Use GET /sign_up for :new
    resources :users, path: 'sign_up', only: [:new, :create], path_names: { new: '' }
    # Use GET /sign_in for :new
    resources :sessions, path: 'sign_in', only: [:new, :create], path_names: { new: '' }
  end

  root 'dashboard#index'
  # Use GET /sign_up for :new
  resources :accounts, path: 'sign_up', only: [:new, :create], path_names: { new: '' }
end
