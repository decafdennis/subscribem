Warden::Strategies.add(:password) do
  def subdomain
    ActionDispatch::Http::URL.extract_subdomain(request.host, 1)
  end

  def valid?
    subdomain.present? && params['user']
  end

  def authenticate!
    user = Subscribem::Account.find_by(subdomain: subdomain).try do |account|
      account.users.find_by(email: params['user']['email'])
    end

    user.try(:authenticate, params['user']['password']) ? success!(user) : fail!
  end
end
