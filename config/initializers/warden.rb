Rails.application.config.middleware.use Warden::Manager do |manager|
  manager.default_strategies :password
  manager.serialize_into_session do |user|
    user.id
  end
  manager.serialize_from_session do |id|
    Subscribem::User.find(id)
  end
end
