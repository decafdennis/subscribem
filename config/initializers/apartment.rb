require 'apartment/elevators/subdomain'

Apartment.configure do |config|
  config.excluded_models = %w(Subscribem::Account Subscribem::Member Subscribem::User)
end

Rails.application.config.middleware.use Apartment::Elevators::Subdomain
